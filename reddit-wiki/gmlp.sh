export EP=`hostname`
python3 pretrain_tlm_tf.py \
       	--basedir gs://tpt3/ckpt-gmlp-reddit-wiki \
	--train_dir gs://tpt3/reddit-wiki/train \
	--valid_dir gs://tpt3/reddit-wiki/valid \
	--mlp true \
	--file_type "tfrecord" \
	--subword_model_file codes.30k \
       	--subword_vocab_file vocab.30k \
	--distribute tpu \
	--tpu_ep ${EP} \
       	--warmup_steps 40000 \
	--batch_size 512 \
	--clip 5.0 \
	--saves_per_epoch 10 \
	--epochs 4 \
	--rpr_k 48 \
	--lr 2.0e-4

