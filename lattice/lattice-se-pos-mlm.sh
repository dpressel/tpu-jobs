export EP=`hostname`
python3 pretrain_mlm_w_se_pos_tf.py \
    --basedir gs://tpt3/ckpt-lattice-se-pos-mlm-8x8 \
    --train_dir gs://tpt3/se-pos-tfr/train \
    --valid_dir gs://tpt3/se-pos-tfr/valid \
    --dataset_key lattice-se-pos-mlm \
    --d_model 512 \
    --num_heads 8 \
    --num_layers 8 \
    --num_train_workers 4 \
    --distribute tpu \
    --nctx 512 \
    --file_type tfrecord \
    --batch_size 32 \
    --subword_vocab_file vocab.200k \
    --dropout 0.1 \
    --optim adamw \
    --lr 2.0e-4 \
    --clip 1.0 \
    --epochs 1 \
    --saves_per_epoch 30 \
    --weight_decay 1.0e-3 \
    --tpu_ep ${EP}

