git clone https://github.com/dpressel/mead-baseline.git
pushd mead-baseline/
pushd layers/
pip3 install --user .
popd
pip3 install --user .[tf2]
pushd mead/api_examples/
gsutil cp gs://tpt3/lattice/vocab.200k vocab.200k
gsutil cp gs://tpt3/lattice/pretrain_mlm_w_pos_tf.py pretrain_mlm_w_pos_tf.py
gsutil cp gs://tpt3/lattice/pretrain_mlm_w_se_pos_tf.py pretrain_mlm_w_se_pos_tf.py

