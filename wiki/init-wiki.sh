git clone https://github.com/dpressel/mead-baseline.git
pushd mead-baseline/
git checkout fix/sp-chars-spm
pushd layers/
pip3 install --user .
pip3 install --user fastBPE
popd
pip3 install --user .[tf2]
pushd api-examples/
gsutil cp gs://tpt3/wiki/codes.30k codes.30k
gsutil cp gs://tpt3/wiki/vocab.30k vocab.30k
