export EP=`hostname`

python3 pretrain_tlm_tf.py \
	--basedir gs://tpt3/ckpt-wiki-pcats-mlm12ra16 \
	--train_dir gs://tpt3/wiki-predict-cats/train \
	--valid_dir gs://tpt3/wiki-predict-cats/valid \
	--file_type "tfrecord" \
	--subword_model_file ./codes.30k \
	--subword_vocab_file ./vocab.30k \
	--distribute tpu \
	--tpu_ep ${EP} \
	--warmup_steps 20000 \
	--saves_per_epoch 2 \
	--epochs 23 \
	--num_heads 12 \
	--num_layers 12 \
	--d_ff 3072 \
	--d_model 768 \
	--lr 1.0e-4 \
	--rpr_k 16 \
	--rpr_value_on false
