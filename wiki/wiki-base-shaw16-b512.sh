export EP=`hostname`

python3 pretrain_tlm_tf.py \
    --basedir gs://tpt3/ckpt-wiki-base-shaw16-bpe-mlm-b512 \
    --train_dir gs://tpt3/wiki-random-bpe-mlm/train \
    --valid_dir gs://tpt3/wiki-random-bpe-mlm/valid \
    --dataset_key wiki-mlm \
    --embed_type default \
    --rpr_value_on false \
    --d_model 768 \
    --num_heads 12 \
    --num_layers 12 \
    --d_ff 3072 \
    --num_train_workers 4 \
    --distribute tpu \
    --nctx 256 \
    --file_type tfrecord \
    --batch_size 512 \
    --subword_model_file codes.30k \
    --subword_vocab_file vocab.30k \
    --subword_type bpe \
    --dropout 0.1 \
    --optim adamw \
    --lr 1.6e-4 \
    --clip 1.0 \
    --epochs 12 \
    --saves_per_epoch 2 \
    --weight_decay 1.0e-2 \
    --rpr_k 16 \
    --tpu_ep ${EP} >& "wiki-base-wiki-shaw16rand.log"

