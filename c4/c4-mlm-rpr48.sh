export EP=`hostname`
python3 pretrain_tlm_tf.py \
    --basedir gs://tpt3/ckpt-dlg-c4-mlm-8x8-rpr48 \
    --train_dir gs://tpt3/c4-mlm-512/train \
    --valid_dir gs://tpt3/c4-mlm-512/valid \
    --dataset_key c4-mlm \
    --embed_type default \
    --rpr_value_on false \
    --d_model 512 \
    --num_heads 8 \
    --num_layers 8 \
    --num_train_workers 4 \
    --distribute tpu \
    --nctx 512 \
    --file_type tfrecord \
    --batch_size 256 \
    --subword_model_file codes.30k \
    --subword_vocab_file vocab.30k \
    --dropout 0.1 \
    --optim adamw \
    --extra_tokens "[CLS]" "[MASK]" "<BOQ>" "<EOQ>" \
    --lr 4.0e-4 \
    --clip 1.0 \
    --epochs 3 \
    --saves_per_epoch 30 \
    --weight_decay 1.0e-3 \
    --rpr_k 48 \
    --tpu_ep ${EP}

