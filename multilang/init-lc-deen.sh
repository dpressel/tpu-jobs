git clone https://github.com/dpressel/mead-baseline.git
pushd mead-baseline/
pushd layers/
pip3 install --user .
pip3 install --user fastBPE
popd
pip3 install --user .[tf2]
pushd mead/api_examples/
gsutil cp gs://tpt3/de-en/codes-lc-deen.50k codes-lc-deen.50k
gsutil cp gs://tpt3/de-en/vocab-lc-deen.50k vocab-lc-deen.50k
