git clone https://github.com/dpressel/mead-baseline.git
pushd mead-baseline/
pushd layers/
pip3 install --user .
pip3 install --user fastBPE
popd
pip3 install --user .[tf2]
pushd api-examples/
gsutil cp gs://tpt3/fr-en/codes.50k codes.50k
gsutil cp gs://tpt3/fr-en/vocab.50k vocab.50k
gsutil cp gs://tpt3/fr-en/codes-lc.50k codes-lc.50k
gsutil cp gs://tpt3/fr-en/vocab-lc.50k vocab-lc.50k
