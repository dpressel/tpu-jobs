export EP=`hostname`
python3 pretrain_transformer_seq2seq_tf.py \
    --basedir gs://tpt3/ckpt-nmt-en-fr-x-nopunc \
    --train_dir gs://tpt3/en-fr/x-nopunc-tfr/train \
    --valid_dir gs://tpt3/en-fr/x-nopunc-tfr/valid \
    --dataset_key en-fr-x-nopunc \
    --embed_type default \
    --d_model 512 \
    --d_ff 2048 \
    --num_heads 8 \
    --num_layers 8 \
    --num_train_workers 4 \
    --distribute tpu \
    --nctx 128 \
    --file_type tfrecord \
    --batch_size 256 \
    --subword_model_file codes.50k \
    --subword_vocab_file vocab.50k \
    --dropout 0.1 \
    --optim adamw \
    --lr 1e-4 \
    --clip 5.0 \
    --epochs 2 \
    --saves_per_epoch 10 \
    --weight_decay 1.0e-3 \
    --rpr_k 16 \
    --tpu_ep ${EP}

