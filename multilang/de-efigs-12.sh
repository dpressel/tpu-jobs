export EP=`hostname`
python3 pretrain_transformer_de_tf.py \
    --basedir gs://tpt3/ckpt-de-figs-en-12 \
    --train_dir gs://tpt3/figs-en/std-tfr/train \
    --valid_dir gs://tpt3/figs-en/std-tfr/valid \
    --dataset_key figs-en \
    --embed_type default \
    --d_model 768 \
    --d_ff 3072 \
    --num_heads 12 \
    --num_layers 12 \
    --num_train_workers 4 \
    --distribute tpu \
    --nctx 128 \
    --file_type tfrecord \
    --batch_size 512 \
    --subword_model_file codes.60k \
    --subword_vocab_file vocab.60k \
    --dropout 0.1 \
    --optim adamw \
    --lr 8e-5 \
    --clip 5.0 \
    --epochs 2 \
    --saves_per_epoch 10 \
    --weight_decay 1.0e-3 \
    --rpr_k 16 \
    --tpu_ep ${EP}

