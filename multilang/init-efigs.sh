git clone https://github.com/dpressel/mead-baseline.git
pushd mead-baseline/
pushd layers/
pip3 install --user .
pip3 install --user fastBPE
popd
pip3 install --user .[tf2]
pushd api-examples/
gsutil cp gs://tpt3/figs-en/codes.60k codes.60k
gsutil cp gs://tpt3/figs-en/vocab.60k vocab.60k
