export EP=`hostname`
for retry in {1..20}; do  
  python3 pretrain_transformer_de_tf.py \
    --basedir gs://tpt3/ckpt-mtm-de-8x8-rpr48 \
    --train_dir gs://tpt3/reddit-forums-mtm-s2s/train \
    --valid_dir gs://tpt3/reddit-forums-mtm-s2s/valid \
    --dataset_key rf-mtm-s2s \
    --embed_type default \
    --d_model 512 \
    --d_ff 2048 \
    --num_heads 8 \
    --num_layers 8 \
    --num_train_workers 4 \
    --distribute tpu \
    --nctx 1024 \
    --file_type tfrecord \
    --loss mutt \
    --reduction_type none \
    --batch_size 64 \
    --subword_model_file codes.30k \
    --subword_vocab_file vocab.30k \
    --extra_tokens "[CLS]" "[MASK]" "<BOQ>" "<EOQ>" \
    --dropout 0.1 \
    --optim adamw \
    --lr 4.0e-4 \
    --clip 1.0 \
    --epochs 3 \
    --saves_per_epoch 10 \
    --weight_decay 1.0e-3 \
    --rpr_k 16 \
    --tpu_ep ${EP} \
    --restart true >& "mtm-de-$retry.log"
  sleep 360
done

