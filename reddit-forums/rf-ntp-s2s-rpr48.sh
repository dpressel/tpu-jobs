export EP=`hostname`
python3 pretrain_transformer_seq2seq_tf.py \
    --basedir gs://tpt3/ckpt-rf-ntp-s2s-8x8-rpr48 \
    --train_dir gs://tpt3/reddit-forums-ntp/train \
    --valid_dir gs://tpt3/reddit-forums-ntp/valid \
    --dataset_key rf-ntp \
    --embed_type default \
    --d_model 512 \
    --d_ff 2048 \
    --num_heads 8 \
    --num_layers 8 \
    --num_train_workers 4 \
    --distribute tpu \
    --nctx 1024 \
    --file_type tfrecord \
    --batch_size 64 \
    --subword_model_file codes.30k \
    --subword_vocab_file vocab.30k \
    --extra_tokens "[CLS]" "[MASK]" "<BOQ>" "<EOQ>" \
    --dropout 0.1 \
    --optim adamw \
    --lr 4.0e-4 \
    --clip 1.0 \
    --epochs 3 \
    --saves_per_epoch 10 \
    --weight_decay 1.0e-3 \
    --rpr_k 48 \
    --tpu_ep ${EP}

