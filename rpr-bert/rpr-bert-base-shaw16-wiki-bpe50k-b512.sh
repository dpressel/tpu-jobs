export EP=`hostname`

python3 pretrain_tlm_tf.py \
    --basedir gs://tpt3/ckpt-rpr-bert-base-shaw16-wiki-bpe50k-mlm-b512 \
    --train_dir gs://tpt3/wiki-random-bpe-50k-mlm/train \
    --valid_dir gs://tpt3/wiki-random-bpe-50k-mlm/valid \
    --dataset_key wiki-mlm \
    --embed_type default \
    --rpr_value_on false \
    --d_model 768 \
    --num_heads 12 \
    --num_layers 12 \
    --d_ff 3072 \
    --num_train_workers 4 \
    --distribute tpu \
    --nctx 256 \
    --file_type tfrecord \
    --batch_size 512 \
    --subword_model_file codes.50k \
    --subword_vocab_file vocab.50k \
    --subword_type bpe \
    --dropout 0.1 \
    --optim adamw \
    --lr 1.6e-4 \
    --clip 1.0 \
    --epochs 12 \
    --saves_per_epoch 2 \
    --weight_decay 1.0e-2 \
    --rpr_k 16 \
    --restart true \
    --tpu_ep ${EP} >& "wiki-bpe50k-shaw16rand.log"

