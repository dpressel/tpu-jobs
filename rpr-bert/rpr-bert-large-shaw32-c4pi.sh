export EP=`hostname`
python3 pretrain_tlm_grad_accum_tf.py \
    --basedir gs://tpt3/ckpt-rpr-bert-large-shaw32-c4pi \
    --train_dir gs://tpt3/c4-pile-spiece-512/train \
    --valid_dir gs://tpt3/c4-pile-spiece-512/valid \
    --dataset_key c4-mlm \
    --embed_type default \
    --rpr_value_on false \
    --d_model 1024 \
    --num_heads 16 \
    --num_layers 24 \
    --d_ff 4096 \
    --num_train_workers 4 \
    --distribute tpu \
    --nctx 512 \
    --file_type tfrecord \
    --batch_size 64 \
    --subword_model_file spiece.model \
    --subword_type sentencepiece \
    --dropout 0.0 \
    --optim adamw \
    --lr 1.0e-4 \
    --clip 1.0 \
    --epochs 1 \
    --warmup_steps 20000 \
    --saves_per_epoch 200 \
    --weight_decay 1.0e-2 \
    --grad_accum 4 \
    --rpr_k 32 \
    --restart true \
    --tpu_ep ${EP} >& "rpr-bert-large-shaw32-c4pi-r.log"

