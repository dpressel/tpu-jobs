git clone https://github.com/dpressel/mead-baseline.git
pushd mead-baseline/
pushd layers/
pip3 install --user . 
popd
pip3 install --user ".[tf2, bpex]"
pushd mead/api_examples/
gsutil cp gs://tpt3/spiece.model spiece.model
gsutil cp gs://tpt3/wiki-random-bpe-50k-mlm/codes.50k .
gsutil cp gs://tpt3/wiki-random-bpe-50k-mlm/vocab.50k .