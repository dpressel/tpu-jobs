export EP=`hostname`
python3 pretrain_tlm_tf.py \
    --basedir gs://tpt3/ckpt-reddit-mlm-8x8-rpr48 \
    --train_dir gs://tpt3/reddit-mlm/train \
    --valid_dir gs://tpt3/reddit-mlm/valid \
    --dataset_key reddit-mlm \
    --embed_type default \
    --d_model 512 \
    --num_heads 8 \
    --num_layers 8 \
    --num_train_workers 4 \
    --distribute tpu \
    --nctx 256 \
    --file_type tfrecord \
    --batch_size 512 \
    --subword_model_file codes.30k \
    --subword_vocab_file vocab.30k \
    --dropout 0.1 \
    --optim adamw \
    --extra_tokens "[CLS]" "[MASK]" "<BOQ>" "<EOQ>" \
    --lr 1.0e-4 \
    --clip 1.0 \
    --epochs 2 \
    --saves_per_epoch 10 \
    --weight_decay 1.0e-3 \
    --rpr_k 48 \
    --tpu_ep ${EP}

