export EP=`hostname`
python3 pretrain_transformer_seq2seq_tf.py \
    --basedir gs://tpt3/ckpt-reddit-mtm-s2s-8x8 \
    --train_dir gs://tpt3/reddit-paired/train \
    --valid_dir gs://tpt3/reddit-paired/valid \
    --dataset_key reddit-paired \
    --embed_type default \
    --d_model 512 \
    --d_ff 2048 \
    --num_heads 8 \
    --num_layers 8 \
    --num_train_workers 4 \
    --distribute tpu \
    --nctx 256 \
    --file_type tfrecord \
    --batch_size 256 \
    --subword_model_file codes.30k \
    --subword_vocab_file vocab.30k \
    --extra_tokens "[CLS]" "[MASK]" "<BOQ>" "<EOQ>" \
    --dropout 0.1 \
    --optim adamw \
    --lr 1.0e-4 \
    --clip 1.0 \
    --epochs 2 \
    --saves_per_epoch 10 \
    --weight_decay 1.0e-3 \
    --rpr_k 16 \
    --tpu_ep ${EP}

